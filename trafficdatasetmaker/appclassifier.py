import json
import os
import logging

logger = logging.getLogger(__name__)

class AppClassifier():
    def __init__(self, pkts_df, app_class_method):
        self.pkts_df = pkts_df
        self.app_class_method = app_class_method

    def classify(self):
        cfier = AppDetectorByPortNumbers(self.pkts_df)
        #else if ndpi ... cfier = AppDetectorByNDpi
        #else if libprotoident ... cfier = AppDetectorByLibprotoident

        cfier.classify()

class AppDetectorByPortNumbers():
    def __init__(self, pkts_df):
        self.pkts_df = pkts_df
    
    def classify(self):
        all_sess = self.pkts_df.l4_sess_idx.dropna().unique().tolist()
        port_nums = json.load(open('%s/portnumbers.json' 
                                    %os.path.dirname(__file__)) )
        for sess_idx in all_sess:
            s_pkts = self.pkts_df.loc[self.pkts_df['l4_sess_idx'] == sess_idx]
            pkt = s_pkts.iloc[0]
            if pkt.l4_type == 'tcp' and pkt.l4_syn == 1 and pkt.l4_ack == 0:
                p_cli, p_srv = [pkt.l4_src_port, pkt.l4_dst_port]
                self.set_app_n_client(sess_idx, p_srv, p_cli, pkt.l4_type)
            else:
                p1, p2 = sorted([pkt.l4_src_port, pkt.l4_dst_port])
                if self.isWellKnownPort(str(p1), port_nums, pkt.l4_type):
                    self.set_app_n_client(sess_idx, p1, p2, pkt.l4_type)
                elif self.isWellKnownPort(str(p2), port_nums, pkt.l4_type):
                    self.set_app_n_client(sess_idx, p2, p1, pkt.l4_type)
                else:
                    self.set_app_n_client(sess_idx, p1, p2, pkt.l4_type)


    def set_app_n_client(self, sess_idx, p_srv, p_cli, l4_type):
        self.pkts_df.loc[self.pkts_df['l4_sess_idx'] == sess_idx, 
                        'app_idx'] = str(p_srv) + l4_type

        self.pkts_df.loc[(self.pkts_df['l4_sess_idx'] == sess_idx) & 
                            (self.pkts_df['l4_src_port'] == p_srv), 
                            'l4_from_client'] = False

        self.pkts_df.loc[(self.pkts_df['l4_sess_idx'] == sess_idx) & 
                            (self.pkts_df['l4_src_port'] == p_cli), 
                            'l4_from_client'] = True

    def isWellKnownPort(self, port, port_nums, conv_type):
        if port in port_nums:
            for proto in port_nums[port]:
                if proto[conv_type] == 'true':
                    return True
        return False

class AppDetectorByNDpi():
    pass

class AppDetectorByLibprotoident():
    pass
