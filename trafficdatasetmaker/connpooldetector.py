import time
import logging
from sklearn.cluster import DBSCAN, KMeans, SpectralClustering

LOGGER = logging.getLogger(__name__)
CONN_POOL_INTERVAL_THRESHOLD = 0.5 #secs

class ConnPoolDetector():
    def __init__(self, pkts_df):
        self.app_pkts = pkts_df

    def detect_conn_pools(self):
        cpools = {}
        usess_idxs = self.app_pkts.user_sess_idx.dropna().unique().tolist()
        for usess_idx in usess_idxs:
            start_stop = {}
            pkts_usess_conn_type = self.app_pkts.loc[ \
                (self.app_pkts['user_sess_idx'] == usess_idx) ]
            unique_conns = pkts_usess_conn_type.l4_sess_idx.dropna().unique().tolist()
            for conn in unique_conns:
                conn_pkts = pkts_usess_conn_type.loc[
                                pkts_usess_conn_type['l4_sess_idx'] == conn]
                start = conn_pkts.iloc[0].rel_time
                end = conn_pkts.iloc[-1].rel_time
                start_stop[conn] = (start, end)
            usses_cpools = self.unionfind_cpools(start_stop)
            for root_conn, conns  in usses_cpools.items():
                self.app_pkts.loc[self.app_pkts['l4_sess_idx'].isin(conns),
                                  'conn_pool_idx'] = root_conn

    def unionfind_cpools(self, start_stop):
        #TODO: optimize with union find
        roots = {} #roots = conn -> pool_root_conn
        roots_conns = {}
        pools = [] #array of arrays sorted
        for cidx, ss in start_stop.items():
            roots[cidx] = cidx
        for cidx1, ss1 in start_stop.items():
            for cidx2, ss2 in start_stop.items():
                if abs(ss2[0] - ss1[0]) < CONN_POOL_INTERVAL_THRESHOLD and \
                    abs(ss2[1] - ss1[1]) < CONN_POOL_INTERVAL_THRESHOLD:
                    if cidx2 < cidx1:
                        for cidx, root in roots.items():
                            if root == cidx1:
                                roots[cidx] = cidx2
                    else:
                        for cidx, root in roots.items():
                            if root == cidx2:
                                roots[cidx] = cidx1
        for cidx, root in roots.items():
            if root not in roots_conns:
                roots_conns[root] = []
            roots_conns[root].append(cidx)
        #for root, cidx_lists in root_conns:
        #    pools.append(sorted(cidx_lists))
        #return sorted(pools, keys=lambda c: c[0])
        return roots_conns