import time
import logging
import pandas as pd
from pcap2csv.pcap2csvutils import Pcap2csvutils

LOGGER = logging.getLogger(__name__)
REQ_BURST_INTERVAL_THRESHOLD = 0.010

class RequestBurstsDetector():
    def __init__(self, pkts_df):
        self.app_pkts = pkts_df

    def detect_request_bursts(self):
        cpools = self.app_pkts.conn_pool_idx.dropna().unique().tolist()
        burst_idx = 0
        for cpool in cpools:
            cpool_pkts = self.app_pkts.loc[
                                self.app_pkts['conn_pool_idx'] == cpool]
            cpool_pdus = Pcap2csvutils().pdus_from_pkts(cpool_pkts)
            cpool_reqs = cpool_pdus.loc[cpool_pdus['l4_from_client']==True]
            cpool_reqs['irt'] = cpool_reqs.epoch_time.diff()
            for idx, req in cpool_reqs.iterrows():
                req_pdu_idx = req.l4_sess_pdu_idx
                if pd.notnull(req['irt']) and req['irt'] > REQ_BURST_INTERVAL_THRESHOLD:
                    burst_idx += 1
                self.app_pkts.loc[self.app_pkts['l4_sess_pdu_idx'] == req_pdu_idx, 'req_burst_idx'] = burst_idx

