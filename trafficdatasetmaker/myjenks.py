import numpy as np
import matplotlib.pyplot as plt
import jenkspy
from kneed import KneeLocator
import jenks_natural_breaks
from sklearn.preprocessing import MinMaxScaler

def goodness_of_variance_fit(array, nclasses):
    classes = jenks_natural_breaks.classify(array, nclasses)
    classes = np.array(classes)
    classified = np.array([classify(i, classes) for i in array])
    maxz = max(classified)
    zone_indices = [[idx for idx, val in enumerate(classified) if zone + 1 == val] for zone in range(maxz)]
    # sum of squared deviations from array mean
    sdam = np.sum((array - array.mean()) ** 2)
    # sorted polygon stats
    array_sort = [np.array([array[index] for index in zone]) for zone in zone_indices]
    # sum of squared deviations of class means
    sdcm = sum([np.sum((classified - classified.mean()) ** 2) for classified in array_sort])
    #print 'sdcm', sdcm
    # goodness of variance fit
    gvf = (sdam - sdcm) / sdam
    return gvf

def classify(value, breaks):
    for i in range(1, len(breaks)):
        if value < breaks[i]:
            return i
    return len(breaks) - 1

def get_cluster_breaks(arr):
    array = np.array(arr)
    #print array
    nclasses = 2
    gvfs = {}
    for nclasses in range(2, min(20, len(array))):
        gvf = goodness_of_variance_fit(array,nclasses)
        gvfs[nclasses] = gvf
        
    gvf, nclasses = get_elbow(gvfs)
    if gvf == None and nclasses == None:
        return None, None, None
    nclasses = int(round(nclasses.flatten().tolist()[0]))
    
    #print array, nclasses
    classes = jenks_natural_breaks.classify(array, nclasses)
    return nclasses, classes, gvf

def get_elbow(gvfs):
    #print (gvfs)
    y, x =gvfs.keys(), gvfs.values()
    #plt.plot(x, y)

    scalerx = MinMaxScaler(feature_range=(0, 1))
    scalery = MinMaxScaler(feature_range=(0, 2))
    x = np.array(x,dtype=np.float32).reshape(-1,1)
    y = np.array(y,dtype=np.float32).reshape(-1,1)
    x = scalerx.fit_transform(x)
    y = scalery.fit_transform(y)
    x = x.flatten().tolist()
    y = y.flatten().tolist()
    
    kneedle = KneeLocator( x, y, S=1.0, curve='convex', direction='increasing')
    #kneedle.plot_knee_normalized()
    #kneedle.plot_knee()
    #print(kneedle.x)
    #print(kneedle.elbow)
    if kneedle.knee == None:
        return [None, None]
    idx =  next(i for i, _ in enumerate(kneedle.x) if np.isclose(_, kneedle.elbow, 0.0000001))
    elbow =[scalerx.inverse_transform(np.array(kneedle.x[idx]).reshape(-1,1)), 
            scalery.inverse_transform(np.array(kneedle.y[idx]).reshape(-1,1))]
    #plt.show()
    return elbow
    
def print_classes(array, classified):
    for grp in get_class_grps(array, classified):
        print(grp)
        
def plot_classes(array, classes):
    plt.scatter(array, [0]*len(array))
    for i in classes[1:-1]:
        plt.axvline(x=i-0.05, color='red')
        
    
    
def get_class_grps(array, classified):
    grps = []
    cgrp = []
    currm = 0
    i = 0
    for m in zip(array, classified):
        if m[1] != currm and i > 0:
            grps.append(cgrp)
            cgrp = []
        i += 1
        cgrp.append(m[0])
        currm = m[1]
    return grps

def get_best_threshold(arr):
    arr_list = arr
    nclasses, class_breaks, gvf = get_cluster_breaks(arr_list)
    if nclasses is None and class_breaks is None and gvf is None:
        return float('inf')
    classified = np.array([classify(i, class_breaks) for i in arr_list])
    #print_classes(arr_list, classified)
    #plot_classes(arr_list, class_breaks)

    allgrps = get_class_grps(arr_list, classified)
    max_div = 0
    max_div_idx = -1
    for i in range(1,len(allgrps)):
        s = allgrps[i-1][-1]
        l = allgrps[i][0]
        div = abs(l/s) if abs(l/s) >= 1 else abs(s/l) 
        if div > max_div:
            max_div, max_div_idx = div, i
            
    best_thresh = (allgrps[max_div_idx-1][-1] + allgrps[max_div_idx][0])/2
    #plt.axvline(x=best_thresh-0.05, color='blue')
    return best_thresh
    
def jenks_1d_pca_clustering(arr):
    raise NotImplementedError
    '''if type(arr[0]) == list:
        #do pca to reduce to 1-d
    arr_list = sorted(arr)
    nclasses, class_breaks, gvf = get_cluster_breaks(arr_list)
    classified = np.array([classify(i, class_breaks) for i in arr_list])
    return classified'''