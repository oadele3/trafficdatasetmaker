# Copyright (c) 2017 University of Houston

import os
import csv
import time
import json
import logging
from io import BytesIO
import numpy as np
import pandas as pd
from pdudetector import PDUDetector
from appclassifier import AppClassifier
from usersessiondetector import UserSessionDetector
from connclassdetector import ConnClassDetector
from connpooldetector import ConnPoolDetector
from requestburstsdetector import RequestBurstsDetector
from pcap2csv.pcap2csv import Pcap2csv
from pcap2csv.pcap2csvutils import Pcap2csvutils

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class trafficdatasetmaker():
    '''class to make csv from capture file'''
    def __init__(self, inputfile, outdir, inputfiletype='packets-csv',
                metadatafile=None, datasets=['packets'], 
                pdu_dtcn_method='heuristic', app_class_method='port_numbers',
                conn_cluster_method='dbscan'):
        allowed_inputfiletypes = ['packets-csv', 'pcap']
        assert (inputfiletype in allowed_inputfiletypes), \
            'value of inputfiletype must be in ' + str(allowed_inputfiletypes)
        self.inputfile = inputfile
        self.outdir = outdir
        self.inputfiletype = inputfiletype
        self.metadatafile = metadatafile
        self.datasets = datasets
        self.pdu_dtcn_method = pdu_dtcn_method
        self.app_class_method = app_class_method
        self.conn_cluster_method = conn_cluster_method
        if not os.path.exists(outdir):
            os.makedirs(outdir)
       
    def makecsvs(self):
        logger.info("started extracting dataset, may take some minutes")
        begintime = time.time()
        #has to be done before any step
        self.make_metadata_if_none() 

        if self.inputfiletype == 'pcap':
            Pcap2csv(self.inputfile, self.outdir, self.metadatafile).makecsvs()
            pkts_df = pd.read_csv(self.outdir + '/all_pkts.csv')
            self.metadata_file = self.outdir + 'metadata.json'
        else: #if inputfiletype == 'packets-csv'
            pkts_df = pd.read_csv(self.inputfile)
        metadat = self.get_metadata()

        logger.info('completed reading packets df at %d secs, now updating df fields',
                     time.time() - begintime)
        pkts_df = self.update_pkts_fields(pkts_df)
        logger.info('completed update_pkt_fields at %d secs, now detecting PDUs',
                     time.time() - begintime)

        
        PDUDetector(pkts_df, self.pdu_dtcn_method).identifyPDU()
        logger.info('completed pdus at %d secs, now detecting application',
                     time.time() - begintime)
        AppClassifier(pkts_df, self.app_class_method).classify()
        logger.info('completed application detection at %d secs, now detecting user sess',
                     time.time() - begintime)
        UserSessionDetector(pkts_df).detect_usessions()
        logger.info('completed user session detection at %d secs, now detecting conn_cluster',
                     time.time() - begintime)
        ConnClassDetector(pkts_df).detect_conn_classes()
        logger.info('completed connection clustering at %d secs, now detecting conn pools',
                     time.time() - begintime)
        ConnPoolDetector(pkts_df).detect_conn_pools()
        logger.info('completed conn pools detection at %d secs, now detecting request bursts',
                     time.time() - begintime)
        RequestBurstsDetector(pkts_df).detect_request_bursts()
        logger.info('completed request bursts detection at %d secs, now writing data sets',
                     time.time() - begintime)

        #metric stats, variance. per app? or per conn_type? which metrics?
        self.make_metric_sds(pkts_df, metadat)

        self.update_metadata_file(pkts_df, metadat)
        self.write_final_datasets(pkts_df)
        logger.info("completed dataset extraction in %d secs", (time.time() - begintime))
    
    def update_pkts_fields(self, pkts_df):
        if len(pkts_df.index) <= 0:
            return
        first_pkt_time = pkts_df.iloc[0].epoch_time
        pkts_df['rel_time'] = pkts_df.epoch_time - first_pkt_time
        ip_addrs = ['l3_src_ip', 'l3_dst_ip']
        pkts_df[ip_addrs] = pkts_df[ip_addrs].fillna(value='')
        pkts_df['user_sess_idx'] = np.nan
        pkts_df['conn_class_idx'] = np.nan
        #pkts_df['conn_pool_id'] = np.nan
        pkts_df['l2_pair_id'] = pkts_df[['l2_src_mac',
                                'l2_dst_mac']].apply(
                                lambda x: '-'.join(sorted(x)), axis=1)
        pkts_df['l3_pair_id'] = pkts_df[['l3_src_ip', 
                                'l3_dst_ip']].apply(
                                lambda x: '-'.join(sorted(x)), axis=1)
        pkts_df['l4_pair_id'] =pkts_df[['l3_src_ip','l3_dst_ip',
                            'l4_src_port','l4_dst_port']].fillna(value='').apply(
                                    lambda x: '-'.join(sorted(
                                        [ ':'.join([x[0],str(x[2])]),
                                         ':'.join([x[1],str(x[3])]) ] )), 
                                    axis=1)
        #pkts_df.loc[pkts_df['global_pdu_idx'].str.contains('nan'), 
        #              'global_pdu_idx'] = ''
        return pkts_df.replace(to_replace=[":-:", '-',
                                            'nan-nan', ], value='')
    
    def make_metadata_if_none(self):
        if self.metadatafile:
            pass
        else:
            self.metadatafile = self.outdir + '/metadata.json'
            dat = {}
            json.dump(dat, open(self.metadatafile, 'w'))

    def get_metadata(self):
        with open(self.metadatafile, 'r') as met:
            dat = json.load(met)
        return dat

    def update_metadata_file(self, pkts_df, dat):
        dat['applications'] = sorted(pkts_df.app_idx.dropna().unique().tolist())
        dat['l4_conversation_classes'] = sorted(pkts_df.conn_class_idx.dropna().unique().tolist())
        dat['l4_conversations'] = sorted(pkts_df.l4_sess_idx.dropna().unique().tolist())
        json.dump(dat, open(self.metadatafile, 'w'))

    def write_final_datasets(self, pkts_df):
        #print(( '*******',self.outdir))
        if 'packets' in self.datasets or 'all' in self.datasets:
            pkts_df.to_csv(self.outdir + '/all_pkts.csv', quoting=csv.QUOTE_NONNUMERIC)
        if 'pdus' in self.datasets or 'pdus_compare_mode' in self.datasets or 'all' in self.datasets:
            pdus_df = Pcap2csvutils().pdus_from_pkts(pkts_df)
            if 'pdus' in self.datasets or 'all' in self.datasets:
                pdus_df['d_size'] = pdus_df['l4_d_size']
                pdus_df.to_csv(self.outdir + '/all_pdus.csv', quoting=csv.QUOTE_NONNUMERIC)
            if 'pdus_compare_mode' in self.datasets or 'all' in self.datasets:
                pdus_compare_df = pdus_df[['rel_time', 'l4_type', 'app_idx', 'l4_sess_idx', 'l3_src_ip', 'l4_src_port', 'l3_dst_ip', 'l4_dst_port','d_size', 'l4_from_client']]
                pdus_compare_df.to_csv(self.outdir + '/pdu_compare.csv', quoting=csv.QUOTE_NONNUMERIC)

    def make_metric_sds(self, pkts_df, metadat):
        metadat['stats'] = {}
        for app_idx, app_pkts in pkts_df.groupby('app_idx'):
            app_pdus = Pcap2csvutils().pdus_from_pkts(pkts_df)
            metadat['stats'][app_idx] = {}
            appstats = metadat['stats'][app_idx]
            pkts_timearr = app_pkts.rel_time

            appstats['packet_l2_size_sd'] = app_pkts.l2_d_size.std()
            appstats['inter_packet_time_sd'] = pkts_timearr.diff().dropna().std()

            pkts_time_cut_range = range(int(pkts_timearr.min()), int(pkts_timearr.max()+1), 1)
            app_pkts['time_cut'] = pd.cut(pkts_timearr, pkts_time_cut_range)
            appstats['pkt_rate_sd'] = app_pkts.groupby('time_cut')['rel_time'].count().std()
            appstats['l2_throughput_std'] = app_pkts.groupby('time_cut')['l2_d_size'].sum().std()

            #appstats['dropped_pkts_rate_std'] = app_pkts.groupby('time_cut')['l4_dropped_pkts'].count().std()
            appstats['fragment_pkts_per_sec_std'] = app_pkts.groupby('time_cut')['l3_is_frag'].count().std()
            appstats['retransmitted_pkts_per_sec_std'] = app_pkts.groupby('time_cut')['l2_d_size'].sum().std()
            appstats['window_size_std'] = app_pkts.l4_win_size.dropna().std()

            appstats['pdu_size_sd'] = app_pdus.l2_d_size.std()
            req_pdus = app_pdus.loc[app_pdus['l4_from_client']==True]
            appstats['inter_req_pdu_time_sd'] = req_pdus.rel_time.diff().dropna().std()

            pdus_timearr = app_pdus.rel_time
            pdus_time_cut_range = range(int(pdus_timearr.min()), int(pdus_timearr.max()+1), 1)
            app_pdus['time_cut'] = pd.cut(pdus_timearr, pdus_time_cut_range)
            appstats['pdu_rate_sd'] = app_pdus.groupby('time_cut')['rel_time'].count().std()
            appstats['l2_throughput_std'] = app_pdus.groupby('time_cut')['l4_d_size'].sum().std()
            

            
            

            

