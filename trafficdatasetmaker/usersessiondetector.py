
import logging
import numpy as np
import pandas as pd
from sklearn.cluster import DBSCAN, KMeans, SpectralClustering
from .myjenks import get_best_threshold, jenks_1d_pca_clustering
from pcap2csv.pcap2csvutils import Pcap2csvutils



LOGGER = logging.getLogger(__name__)

class UserSessionDetector():
    def __init__(self, pkts_df):
        self.pkts_df = pkts_df

    def detect_usessions(self):
        LOGGER.debug('calculating user sessions by pkts')
        assert (self.pkts_df is not None), 'pkts dataframe cannot be None'
        apps = self.pkts_df.app_idx.dropna().unique().tolist()
        for app_name in apps:
            app_pkts = self.pkts_df.loc[self.pkts_df['app_idx'] == app_name]
            LOGGER.debug('getting users sessions for app %s', str(app_name))
            conns = app_pkts.l4_sess_idx.dropna().unique().tolist()
            if len(conns) <= 1:
                self.pkts_df.loc[self.pkts_df['app_idx'] == app_name, 
                             'user_sess_idx'] = app_name + '_0'
                continue

            pre_conn_gaps = self.get_pre_conn_gaps(app_name, app_pkts)
            usess_gap_thresh = self.get_usess_gap_threshold(app_name, pre_conn_gaps)
            endpt_pairs = app_pkts.l3_pair_id.dropna().unique().tolist()
            curr_usess_id = -1
            LOGGER.debug('%d l3 endpoint pairs found for app %s',
                         len(endpt_pairs), str(app_name))
            for endpt_pair in endpt_pairs:
                app_ep_pkts = app_pkts.loc[
                    app_pkts['l3_pair_id'] == endpt_pair]
                app_ep_conns = app_ep_pkts.l4_sess_idx.dropna().unique().tolist()
                curr_usess_id += 1
                for conn_id in app_ep_conns:
                    if conn_id not in pre_conn_gaps:
                        continue
                    if pre_conn_gaps[conn_id] > usess_gap_thresh:
                        curr_usess_id += 1
                    #print 'conn, usess', conn_id, curr_usess_id
                    self.pkts_df.loc[self.pkts_df['l4_sess_idx'] == conn_id,
                                    'user_sess_idx'] = app_name + '_' + str(curr_usess_id)
            LOGGER.debug('%d usersessions found for app %s',
                        curr_usess_id + 1, str(app_name))


    def get_pre_conn_gaps(self, app_name, app_pkts):
        #this is an alg... need to implement...
        #LOGGER.debug( 'getting get_pre_conn_gaps for app %s',
        #             str(self.app_name))
        pre_conn_gaps = {}
        endpt_pairs = app_pkts.l3_pair_id.dropna().unique().tolist()
        #print('hereee', self.app_name, app_pkts)
        for endpt_pair in endpt_pairs:
            app_ep_pkts = app_pkts.loc[
                app_pkts['l3_pair_id'] == endpt_pair]
            app_ep_conns = app_ep_pkts.l4_sess_idx.dropna().unique().tolist()
            t_last_conn_end = 0 #app_ep_pkts.iloc[-1].epoch_time
            for conn_id in app_ep_conns:
                conn_id = int(conn_id)
                #print(conn_id)
                conn_pkts = app_ep_pkts.loc[app_ep_pkts['l4_sess_idx'] == conn_id]
                conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
                LOGGER.debug('conn %d has %d packets and %d pdus', conn_id,
                             len(conn_pkts.index), len(conn_pdus.index))
                if len(conn_pdus.index) == 0:
                    #below okay, since its for connections with no pdus
                    #pre_conn_gaps[conn_id] = 0
                    continue
                conn_start = conn_pdus.iloc[0].epoch_time
                conn_end = conn_pdus.iloc[-1].epoch_time
                gap = max(0, conn_start - t_last_conn_end)
                pre_conn_gaps[conn_id] = gap
                t_last_conn_end = max(conn_end, t_last_conn_end)
        #print(pre_conn_gaps)
        LOGGER.debug('length of pre_conn_gaps for app %s is %d', 
                    str(app_name), len(pre_conn_gaps))
        return pre_conn_gaps

    def get_usess_gap_threshold(self, app_name, pre_conn_gaps):
        LOGGER.debug( 'getting usess_gap_threshold for app %s',
                    str(app_name))
        dset_usess_gap = []
        for g in pre_conn_gaps.values():
            if g > 0:
                dset_usess_gap.append(g)
        # min user time betw sess should be greater than human response time...
        LOGGER.debug('len dset_usess_gap is %d ',
                    len(dset_usess_gap))
        if len(dset_usess_gap) <= 3:
            return float('inf')
        usess_gap_thresh = max(0.5, get_best_threshold(dset_usess_gap))
        LOGGER.debug('usess_gap_threshold for app %s is %f',
                    str(app_name), usess_gap_thresh)
        return usess_gap_thresh