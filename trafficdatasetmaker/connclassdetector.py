import time
import logging
import numpy as np
from sklearn.cluster import DBSCAN, KMeans, SpectralClustering
from .myjenks import get_best_threshold, jenks_1d_pca_clustering
from pcap2csv.pcap2csvutils import Pcap2csvutils

LOGGER = logging.getLogger(__name__)

class ConnClassDetector():
    def __init__(self, pkts_df):
        self.pkts_df = pkts_df

    def detect_conn_classes(self):
        utilizer = Pcap2csvutils()
        apps = self.pkts_df.app_idx.dropna().unique().tolist()
        for app_name in apps:
            app_pkts = self.pkts_df.loc[self.pkts_df['app_idx'] == app_name]
            LOGGER.debug('getting get_conn_classes for app %s', str(app_name))
            conns = app_pkts.l4_sess_idx.dropna().unique().tolist()
            if len(conns) <= 1:
                self.pkts_df.loc[self.pkts_df['app_idx'] == app_name, 
                             'conn_class_idx'] = app_name + '_0'
                continue
            conn_class_dset = []
            for conn in conns:
                conn_pkts = app_pkts.loc[app_pkts['l4_sess_idx'] == conn]
                conn_pdus = utilizer.pdus_from_pkts(conn_pkts)
                if len(conn_pdus.index) == 0:
                    #conn_class_dset.append([0,0,0,0,0])#, 0])
                    conn_class_dset.append([0])
                    continue
                dur = conn_pdus.iloc[-1].epoch_time - conn_pdus.iloc[0].epoch_time
                #u_sess_idx = conn_pkts.iloc[0].user_sess_idx
                #u_sess_pkts = app_pkts.loc[app_pkts['user_sess_idx']==u_sess_idx]
                #u_sess_starttime = u_sess_pkts.iloc[0].epoch_time
                #conn_start_time_in_sess = conn_pdus.iloc[0].epoch_time - u_sess_starttime
                pdus_cli = conn_pdus.loc[(conn_pdus['l4_sess_idx'] == conn) &
                                        (conn_pdus['l4_from_client'] == True)]
                pdus_srv = conn_pdus.loc[(conn_pdus['l4_sess_idx'] == conn) &
                                        (conn_pdus['l4_from_client'] == False)]
                num_pdus_cli = len(pdus_cli.index)
                num_pdus_srv = len(pdus_srv.index)
                total_data_cli = pdus_cli.l4_d_size.sum()
                total_data_srv = pdus_srv.l4_d_size.sum()
                #conn_class_dset.append([dur, num_pdus_cli, #conn_start_time_in_sess, < this makes v slow...
                #                        num_pdus_srv, total_data_cli, 
                #                        total_data_srv])
                conn_class_dset.append([num_pdus_cli])
            X = np.array(conn_class_dset)
            dbscan_res = DBSCAN(eps=3, min_samples=2).fit(X).labels_
            kmeans_res = KMeans(n_clusters=2, random_state=0).fit(X).labels_
            #sc_res = SpectralClustering(n_clusters=2, assign_labels="discretize",
            #                random_state=0).fit(X).labels_
            #jenk_res = jenks_1d_pca_clustering(X.tolist())
            best_res = utilizer.get_bestclustering(dbscan_res, kmeans_res) #\, sc_res
            best_res = kmeans_res
            #, jenk_res)
            best_res = best_res.flatten().tolist()
            #print(zip(X.flatten().tolist(), best_res))lass_dset.append([ num_pdus_cli])

            for conn, conn_class in zip(conns, best_res):
                #print 'conn, conn_class', conn, conn_class
                self.pkts_df.loc[self.pkts_df['l4_sess_idx'] == conn, 
                             'conn_class_idx'] = app_name + '_' + str(conn_class)
            LOGGER.debug('%d conn_classes found for app %s ',
                        len(set(best_res)), str(app_name))


