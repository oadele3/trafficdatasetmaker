import json
import os
import logging

logger = logging.getLogger(__name__)

PDU_TIME_INTERVAL_THRESHOLD = 0.5

class PDUDetector():
    def __init__(self, pkts_df, pdu_dtcn_method):
        self.pkts_df = pkts_df
        self.pdu_dtcn_method = pdu_dtcn_method

    def identifyPDU(self):
        cfier = PDUDetectorByHeuristic(self.pkts_df)
        #else if pdu_dtcn_method == 'other?' ... cfier = AppDetectorByOther

        cfier.identifyPDU()

class PDUDetectorByHeuristic():
    def __init__(self, pkts_df):
        self.pkts_df = pkts_df

    def identifyPDU(self):
        all_sess = self.pkts_df.l4_sess_idx.dropna().unique().tolist()
        intval = PDU_TIME_INTERVAL_THRESHOLD
        for sess_idx in all_sess:
            s_pkts = self.pkts_df.loc[self.pkts_df['l4_sess_idx'] == sess_idx]
            ep1 = (s_pkts.iloc[0].l3_src_ip, str(int(s_pkts.iloc[0].l4_src_port)))
            ep2 = (s_pkts.iloc[0].l3_dst_ip, str(int(s_pkts.iloc[0].l4_dst_port)))
            
            maxi = {ep1:s_pkts.loc[(s_pkts['l3_src_ip']==ep1[0]) & (s_pkts['l4_src_port']==float(ep1[1])) ].l4_d_size.dropna().max(),
                    ep2:s_pkts.loc[(s_pkts['l3_src_ip']==ep2[0]) & (s_pkts['l4_src_port']==float(ep2[1])) ].l4_d_size.dropna().max()}
            print(maxi)
            last_src = (s_pkts.iloc[0].l3_dst_ip, str(int(s_pkts.iloc[0].l4_dst_port)))
            last_time = s_pkts.iloc[0].rel_time - 10*intval
            last_size = 0
            curr_pdu_idx = 0
            for i, pkt in s_pkts.iterrows():
                if pkt['l4_d_size'] == 0:# and pkt['l4_type'] == 'tcp':
                    continue
                src_ep = (pkt['l3_src_ip'], str(int(pkt['l4_src_port'])))
                if ((src_ep != last_src) or
                  ((pkt['rel_time'] - last_time) > intval) or
                  (last_size < maxi[src_ep])):
                    curr_pdu_idx += 1
                pkt_idx = pkt['idx']
                self.pkts_df.loc[self.pkts_df['idx']==pkt_idx, \
                    'l4_sess_pdu_idx'] = curr_pdu_idx
                last_time = pkt['rel_time']
                last_src = src_ep
                last_size = pkt['l4_d_size']
        self.pkts_df['global_pdu_idx'] = self.pkts_df.l4_sess_idx.map(str) + '_' + \
            self.pkts_df.l4_sess_pdu_idx.map(str)

class AppDetectorByOther():
    pass