from setuptools import setup, find_packages

setup(name='trafficdatasetmaker',
      version='0.0.0.0',
      description='''creates dataset csvfile with processed 
      classified information packets and connections in a pcap file,
      using some clustering methods, and heuristics''',
      url='https://bitbucket.org/oadele3/trafficdatasetmaker',
      author='UH-Netlab, Oluwamayowa Adeleke',
      author_email='oluwamayowa.adeleke@gmail.com',
      license='MIT',
      #packages=['trafficdatasetmaker'],
      packages=find_packages(),
      scripts=['bin/trafficdatasetmaker'],
      include_package_data=True,
      zip_safe=False,
      install_requires=['pandas', 'numpy', 'sklearn', 'jenkspy', 'jenks_natural_breaks', 'kneed<=0.5.0'],
      classifiers = [
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        ]
      )
